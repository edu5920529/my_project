### myproject
Курс SkillFactory DEVOPS. Дипломный проект. Инфраструктура, CI/CD, мониторинг для приложения на Django + PostgreSQL.

Дополнительный репозиторий  https://gitlab.com/edu5920529/script.git

### Спринт №1
- Требуется описать инфраструктуру будущего проекта в виде кода с инструкциями по развертке, нужен кластер Kubernetes и служебный сервер srv.
- В качестве облака используется Яндекс.Облако.
- Необходимо три сервера: два сервера в одном кластере Kubernetes - 1 master и 1 app. Сервер srv для инструментов мониторинга, логгирования и сборок контейнеров
- Создание инфраструктуры и установка ПО должны быть автоматизированы

### Решение
Для реализации задачи использовался Terraform.


- **terraform**/
- 
- ├── **keys**
- │   ├── authorized_key.json
- │   ├── gitlab-runner_docker.token
- │   ├── gitlab-runner_k8s.token
- │   ├── terraform
- │   └── terraform.pub
- ├── main.tf
- ├── **modules**
- │   ├── **vm**
- │   │   ├── outputs.tf
- │   │   ├── provider.tf
- │   │   ├── provisioning_k8s.tf
- │   │   ├── provisioning_srv.tf
- │   │   ├── variables.tf
- │   │   └── vm.tf
- │   └── **vpc**
- │       ├── outputs.tf
- │       ├── provider.tf
- │       ├── variables.tf
- │       └── vpc.tf
- ├── outputs.tf
- ├── provider.tf
- ├── terraform.tfstate
- └── terraform.tfstate.backup


- vm.tf - создание ВМ. Количество ВМ, образ системы и пути до ключей указывается в файле modules/vm/variables.tf
- vpc.tf - создание сети. Имя сети, имя подсети, пул ip v4, зона датацентра и ДНС сервер указывается в файле modules/vpc/variables.tf
- provisioning_k8s.tf - установка Kubernetes кластера и Gitlab-runner. Kubernetes кластер устанавливается Kubeadm-ом. На одной из Control ноде происходит инициализация кластера. Все Worker ноды добавляются в кластер автоматически. Остальные Control ноды в данном релизе нужно добавить руками. Файл с командой для добавления нод в кластер создаётся на машине администратора в каталоге /tmp/kubeadm-init.out.
Для автоматической регистрации Gitlab-runner необходимо, предварительно, создать его на Gitlab. Токен помещается в каталог keys/gitlab-runner_k8s.token.
Для упрощения написания provisioning_k8s.tf использовались скрипты из репозитория https://gitlab.com/edu5920529/script.git
- provisioning_srv.tf - установка Docker, Docker-Compose и Gitlab-runner. Регистрация Gitlab-runner так же происходит автоматически, нужно предварительно создать его на Gitlab и поместить токен в каталог keys/gitlab-runner_docker.token.


### Спринт №2
- Необходимо написать пайплайн для сборки образа из Dockerfile и отправки его в docker registry
- Необходимо описать приложение в виде конфигов в Helm-чарт и настроить деплой стадию пайплайна. Для деплоя должен использоваться свежесобранный образ

### Решение
Для реализации задачи использовался GitLab CI/CD. Используем два Gitlab-runner: один с тэгом docker на сервере srv для сборки и отправки образа, второй с тэгом kubernetes на сервере master для деплоя в кластер

Предварительно настраиваем токен доступа для отправки образа в Gitlab Container Registry:
- Settings - Repository - Deploy tokens - Add token
- Создаём токен, указываем имя, указываем необходимые настройки.
- Далее Username токена и сам токен записываем в Variables:
- Settings - CI/CD - Variables - Add variables
- В моём случае:
- CI_DEPLOY_USER - Username токена
- CI_DEPLOY_PASSWORD - значение самого токена


Создаём файл .gitlab-ci.yml, настраиваем в нём пайплайн

Для запуска пайплайна вручную используем workflow rules

Описываем переменные:
- IMAGE_TAG - тэг образа, принимает значение встроенных в Gitlab переменных CI_REGISTRY_IMAGE и CI_COMMIT_SHORT_SHA. В моём случае IMAGE_TAG выглядит следующим образом:registry.gitlab.com/edu5920529/myproject:5d6887ed
- PROJECT_NAME - имя проекта, принимает значение встроенной в Gitlab переменной $CI_PROJECT_NAME. В моём случае PROJECT_NAME: myproject
- REPLICAS - количество реплик подов, указывается вручную
- NAMESPACE - NAMESPACE приложения в Kubermnetes

Далее настраиваем стадию сборки образа из Dockerfile и отправки его в в Gitlab Container Registry. Используем ранее описанные переменные. CI_DEPLOY_USER и CI_DEPLOY_PASSWORD - это наш токен, CI_REGISTRY - встроенная переменная, значение registry.gitlab.com

Затем настраиваем стадию деплоя. Предварительно формируем файл values.yaml из переменных. Переменные для настройки и подключения к БД хранятся в Settings - CI/CD - Variables. В дальнейшем значения из values.yaml будут переданы в манифест. Создаём Namespace через манифест. Если такой Namespace уже существует, то система выдаст уведомление unchange, без ошибок.

### Спринт №3 - вишенка на торте
- Необходимо настроить мониторинг и сборку логов

### Решение
Для реализации задачи по мониторингу использовался стэк Prometheus. Для сборки логов Grafana Loki, Promtail

- Prometheus запускается (собирается) на сервере srv с помощью docker-compose файла
- В Grafana вручную подключаются Prometheus и Grafana Loki. Настраивается дашборд
- В кластер, с помощью helm, устанавливается Node-exporter из репозитория helm repo add prometheus-community https://prometheus-community.github.io/helm-charts. При запуске helm Node-exporter автоматически устанавливается на каждую ноду. Для Node-exporter создаётся сервис, по умолчанию тип ClusterIp. Перенастраиваем тип сервиса на Node-Port через --set или --values (нужно, чтобы Prometheus смог зайти на Node-exporter и собрать метрики)
- В кластер, с помощью helm, устанавливается Promtail из репозитория helm repo add grafana https://grafana.github.io/helm-charts. При запуске helm Promtail автоматически устанавливается на каждую ноду. В качествет параметров через --set или --values указывается адрес Grafana Loki (нужен, чтобы Promtail смог отправить логи на Grafana Loki сервер)
- Для jobs мониторинга и сборки логов настроены rules и в начале пайплайна определены переменные, которые могут принимать значения true или false. Если выставлено значение false, то jobs мониторинга и сборки логов не будут запускаться при запуске пайплайна

Telegram автора: t.me/AndreevL

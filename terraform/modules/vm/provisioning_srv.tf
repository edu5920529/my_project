# Ubuntu 20.04 LTS
#
#
#
# Install Docker
#
resource "null_resource" "srv-install" {
  count = var.srv
  depends_on = [yandex_compute_instance.srv]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.srv[count.index].network_interface.0.nat_ip_address
  }

  provisioner "remote-exec" {
    inline = [
    "sudo apt-get update -y",
    "wget -O - https://gitlab.com/edu5920529/script/-/raw/main/10_ubuntu_docker_install.sh | bash",
    "sleep 10"
	]
  }
}

#
# Gitlab-runner
#
resource "null_resource" "srv-gitlab-runner" {
  count = var.srv
  depends_on = [null_resource.srv-install]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.srv[count.index].network_interface.0.nat_ip_address
  }

  provisioner "file" {
    source      = "${var.gitlab_tokens.docker_token}"
    destination = "gitlab-runner.token"
  }
  
  provisioner "remote-exec" {
    inline = [
    "wget -O - https://gitlab.com/edu5920529/script/-/raw/main/20_ubuntu_gitlab-runner_install.sh | bash",
    "sudo usermod -aG docker gitlab-runner",
    "sleep 10",
    "wget -O - https://gitlab.com/edu5920529/script/-/raw/main/30_gitlab-runner_register.sh | bash",
    "sleep 10"
	]
  }
}

#
# test
#
resource "null_resource" "srv-install-test" {
  count = var.srv
  depends_on = [null_resource.srv-install, null_resource.srv-gitlab-runner]
  connection {
    user        = var.ssh_credentials.user
    private_key = file(var.ssh_credentials.private_key)
    host        = yandex_compute_instance.srv[count.index].network_interface.0.nat_ip_address
  }

  provisioner "remote-exec" {
    inline = [
    "docker --version > /tmp/srv${count.index}_docker_version",
    "docker-compose --version > /tmp/srv${count.index}_docker_compose_version",
    "gitlab-runner --version > /tmp/srv${count.index}_gitlab-runner_version",
    "sudo cp /etc/gitlab-runner/config.toml /tmp/srv${count.index}_gitlab-runner_config.toml",
    "sudo chmod +r /tmp/srv${count.index}_gitlab-runner_config.toml"
  ]
  }
  
  provisioner "local-exec" {
    command = "scp -i ${var.ssh_credentials.private_key} -o StrictHostKeyChecking=no ${var.ssh_credentials.user}@${yandex_compute_instance.srv[count.index].network_interface.0.nat_ip_address}:/tmp/srv${count.index}_docker_version /tmp/srv${count.index}_docker_version"
  }

  provisioner "local-exec" {
    command = "scp -i ${var.ssh_credentials.private_key} -o StrictHostKeyChecking=no ${var.ssh_credentials.user}@${yandex_compute_instance.srv[count.index].network_interface.0.nat_ip_address}:/tmp/srv${count.index}_docker_compose_version /tmp/srv${count.index}_docker_compose_version"
  }

  provisioner "local-exec" {
    command = "scp -i ${var.ssh_credentials.private_key} -o StrictHostKeyChecking=no ${var.ssh_credentials.user}@${yandex_compute_instance.srv[count.index].network_interface.0.nat_ip_address}:/tmp/srv${count.index}_gitlab-runner_version /tmp/srv${count.index}_gitlab-runner_version"
  }

  provisioner "local-exec" {
    command = "scp -i ${var.ssh_credentials.private_key} -o StrictHostKeyChecking=no ${var.ssh_credentials.user}@${yandex_compute_instance.srv[count.index].network_interface.0.nat_ip_address}:/tmp/srv${count.index}_gitlab-runner_config.toml /tmp/srv${count.index}_gitlab-runner_config.toml"
  }
}